##
## node.py - Parse tree node class
## (c) Remy Horton, 2009
##
# {{{ Licence
# This file is part of LyteCFG
# SSI.cgi is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SSI.cgi is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LyteCFG.  If not, see <http://www.gnu.org/licenses/>.
# }}}


# Whereas in the GUI the TreeCtrl maintains tree structure information, the
# parser output has to be self-contained. Since inserting the parse tree into
# the TreeCtrl involves a full traversal anyway, i find it easier to maintain
# seperate classes optimised for the task at hand rather than trying to bolt
# the two together.


class Node:
    def __init__(self,value):
        self.value = value
    def __repr__(self):
        return "Node: " + str(self.value)
    def __str__(self):
        return str(self.value)
    
    def expand(self,list):
        union = []
        for elem in list:
            union.append(str(elem))
        return "+".join(union)

class StringNode(Node):
    def __repr__(self):
        return "StringNode:" + self.value
    def __str__(self):
        return "\"" + self.value + "\""
    
class NumberNode(Node):
    def __repr__(self):
        return "NumberNode:" + str(self.value)
    
class NameNode(Node):
    def __repr__(self):
        return "NameNode:" + self.value

class ArrayNode(Node):
    def __repr__(self):
        return "ArrayNode:" + str(self.value)
    
    def __str__(self):
        entries = []
        for value in self.value:
            (key,vals) = value
            values = self.expand(vals)
            if key != "":
                entries.append( str(key) + " => " + values )
            else:
                entries.append( values )
        return "(" + ", ".join(entries) + ")"

class AssignNode(Node):
    def __init__(self,name,value):
        Node.__init__(self,value)
        self.name = name
    def __repr__(self):
        values = []
        for v in self.value:
            values.append( str(v) )
        return "AssignNode: "+self.name+"=" + "+".join(values)
    def __str__(self):
        values = []
        for v in self.value:
            values.append( str(v) )
        strn = self.name + " = " + "+".join(values)
        return strn

class AppendNode(Node):
    def __init__(self,name,value):
        Node.__init__(self,value)
        self.name = name
    def __str__(self):
        values = []
        for v in self.value:
            values.append( str(v) )
        strn = self.name + " += " + "+".join(values)
        return strn
    
class IncludeNode(Node):
    pass

class CondNode(Node):
    def __init__(self,field,cond,value,payload):
        Node.__init__(self,value)
        self.field = field
        self.cond  = cond
        self.block = payload
    def __repr__(self):        
        blk = []
        for stmt in self.block:
            blk.append(str(stmt))
        return "" + \
               self.field + " " + self.cond + " " + str(self.value) + \
               "  { \n" + "\n".join(blk) + "\n}"
    def __str__(self):
        return self.field + " " + self.cond + " " + self.expand(self.value)
