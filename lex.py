##
## lex.py - Lexical analysis (tokenisation)
## (c) Remy Horton, 2009
##
# {{{ Licence
# This file is part of LyteCFG
# SSI.cgi is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SSI.cgi is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LyteCFG.  If not, see <http://www.gnu.org/licenses/>.
# }}}












# Lexical analysis error. Mostly fatal errors, but also used when the source
# file contains an unparsbale character.
class LexError(Exception):
    pass
#    def __init__(self,msg):
#        self.message = msg
#    def __str__(self):
#        return self.message



# Token class. 
class Token:
    # Token types
    Assign=1
    Arrow=2
    Eq=3
    IsExp=4
    NoExp=5
    PlusEq=6
    Plus=7
    Ne=8
    Dollar=9
    Var=10
    Num=11
    String=12
    Bool=13
    Field=14
    OpenBr=15
    OpenPa=16
    CloseBr=17
    ClosePa=18   
    EOF=19
    Comment=20
    Comma=21

    # Creator function
    def __init__(self,tokentype,tokPos,val):
        self.tokType = tokentype
        (self.line,self.column) = tokPos
        self.value = val;


    # 'Show' function
    def __str__(self):
        try:
            tokenStr = {
                self.Var     : "Name",
                self.Comment : "Comment",
                self.EOF     : "EOF",
                self.String  : "String",
                self.Num     : "Number",
                self.Eq      : "Eq",
                self.Arrow   : "Arrpw",
                self.Assign  : "Assign",
                self.OpenBr  : "OpenBr",
                self.CloseBr : "CloseBr",
                self.Comma   : "Comma",
                self.OpenPa  : "OpenPa",
                self.ClosePa : "ClosePa",
                self.Plus    : "Plus",
                self.PlusEq  : "PlusEq",
                self.Field   : "Field",
                self.OpenBr  : "OpenBr",
                self.CloseBr : "CloseBr",
                self.IsExp   : "IsExp",
                self.NoExp   : "NoExp"
                }[self.tokType]
            return tokenStr + \
                   "("+str(self.line)  + \
                   ","+str(self.column)+ \
                   "): "+str(self.value)
        except KeyError:
            return "Unknown: " + str(self.tokType)



## Lexical analyser.
# Apart from a single-character buffer to allow for some lookahead, the file
# is only read as-needed. This approach is somewhat dated as these days it is
# better to read the file into a buffer in one go. 
class Lex:
    # Initaliser
    def __init__(self):
        self.sourceFP = None
        self.currChar = None;
        self.onNewLine = False;
        self.currentLine = 1;
        self.currentCol  = 0;
        self.tokenStart = (0,0);
        self.seenEOF = 0

    # Opens a source file for tokenisation
    def openSource(self,fileName):
        try:
            self.sourceFP = open(fileName,"r")
        except:
            raise LexError("Unable to open '"+fileName+"'")

    # Read in and discard surplus whitespace
    def eatWhitespace(self):
        while True:
            char = self.peekChar()
            if char == "":
                return
            if not char.isspace():
                return
            self.nextChar()


    # Consumes whitespace from the source file. 
    def tokenComment(self):
        commentText = []
        char = self.peekChar();
        while True:
            ##print "EatC: " + char
            char = self.nextChar();
            if char == '\n' or char == '\r' or char == "":
                return Token(Token.Comment, \
                             self.tokenStart,\
                             "".join(commentText))
            commentText.append(char)


    # Read in a string token
    def tokenString(self):
        stringText = []
        char = self.nextChar();
        #print "tokenString(): '" + char + "'"       
        while True:
            char = self.nextChar(); 
            #print "tokenString(): '" + char + "'"
            if char == "":
                raise LexError(self.posStr() + ": " + 
                               "Reached end of file looking for '\"'")
            if char == '"':
                return Token(Token.String,self.tokenStart,"".join(stringText))
            stringText.append(char)


    # Read in a 'name' token
    def tokenVar(self):
        varName = []
        varName.append(self.nextChar())
        while True:
            char = self.peekChar();
            if not (char.isalnum() or char=='-' or char=='.') or char == "":
                return Token(Token.Var, self.tokenStart,"".join(varName))
            varName.append(self.nextChar())


    # Read in a 'Field' token.    
    def tokenField(self):
        fieldName = []
        fieldName.append(self.nextChar())
        while True:
            char = self.peekChar();
            if not (char.isalnum() or \
                    char=='['  or  char==']' or \
                    char=='"' ) \
                    or char == "":
                return Token(Token.Field, self.tokenStart,"".join(fieldName))
            fieldName.append(self.nextChar())

        
#        return None; #FIXME

            
    # Reads in a number (integer) token
    def tokenNumber(self):
        numStr = []
        while True:
            char = self.peekChar();
            if not char.isdigit() or char == "":
                return Token(Token.Num, self.tokenStart,"".join(numStr))
            numStr.append(self.nextChar())
        return Token(Token.Num,0,0,"");


    # Handle tokens that start with '='
    def tokenEq(self):
        self.nextChar()
        try:
            tokType = {
                '=' : Token.Eq,
                '>' : Token.Arrow,
                '~' : Token.IsExp
                }[self.peekChar()]
            return Token(tokType, self.tokenStart, self.nextChar())
        except KeyError:
            return Token(Token.Assign, self.tokenStart, self.peekChar())
        


    # Handle tokens that start with '+'
    def tokenPlus(self):
        self.nextChar()
        if self.peekChar() == '=':
            self.nextChar()
            return Token(Token.PlusEq, self.tokenStart, "+=")
        else:
            return Token(Token.Plus  , self.tokenStart, "+") 


    # Handle tokens that start with '!'
    def tokenBang(self):
        self.nextChar()
        char = self.peekChar()
        if clar == '=':
            self.nextChar()
            return Token(Token.Ne, self.tokenStart, "!=")
        elif char == '~':
            self.nextChar()
            return Token(Token.NoExp, self.tokenStart, "!~")
        else:
            raise LexError(self.posStr() + ": " + 
                           "Expected '~' or '=' after '!';" + \
                           "found '" + char + "'")


    # Used for creation of tokens that corresponds to single characters which
    # are also not prefixes of longer tokens. 
    def tokenSingleChar(self,tokType,tokStr):
        self.nextChar()
        return Token(tokType,self.tokenStart,tokStr)


    # Internal function used by peekChar and nextChar (does the actual work).
    # A single-character buffer is used to implement lookahead
    def readChar(self,peek):
        # Read in char if not already cached
        if self.currChar == None:
            currentChar = self.sourceFP.read(1)
            self.currentCol = self.currentCol + 1
            if self.onNewLine:
                self.currentLine = self.currentLine + 1
                self.currentCol  = 0
            self.onNewLine = False;
            if currentChar == '\n':
                self.onNewLine = True
            self.currChar = currentChar
        else:
            currentChar = self.currChar
        # Delete cache if non-peek
        if peek == False:
            self.currChar = None;
        return currentChar

    # Peek at next input character
    def peekChar(self):
        return self.readChar(True)

    # Get next input char and mark it as read
    def nextChar(self):
        return self.readChar(False)

    def posStr(self):
        line,col = self.tokenStart
        #line,col = self.currentLine,self.currentCol
        return "Line " + str(line) + " Col " + str(col)

    # Gets the next input token. Based on the first character it calls one of
    # the internal token*() functions. By convention the called token*()
    # function consumes the character rather than this function.
    def getToken(self):
        if self.sourceFP == None:
            raise LexError("getToken(): No source file opened")
        if self.seenEOF:
            raise LexError("getToken(): Multiple EOF reads")
        self.eatWhitespace();
        self.tokenStart = (self.currentLine,self.currentCol)
        char = self.peekChar();
#        print (">>>>" + str(len(char))  + "<<<<")
        try:
            if char == "":
                self.seenEOF = True
#                print ("EOF")
                return Token(Token.EOF,self.tokenStart,"EOF")
            elif char.isdigit():
                token = self.tokenNumber()
            elif char.isalpha():
                token = self.tokenVar();
            else:
                jumpTable = {
                    '#' : self.tokenComment,
                    '"' : self.tokenString,
                    '$' : self.tokenField,
                    '=' : self.tokenEq,
                    '!' : self.tokenBang,
                    '+' : self.tokenPlus,
                    '{' : lambda : self.tokenSingleChar(Token.OpenBr,"{"),
                    '}' : lambda : self.tokenSingleChar(Token.CloseBr,"}"),
                    '(' : lambda : self.tokenSingleChar(Token.OpenPa,"("),
                    ')' : lambda : self.tokenSingleChar(Token.ClosePa,")"),
                    ',' : lambda : self.tokenSingleChar(Token.Comma,",")
                    }
                token = jumpTable[char]()
            return token
        except KeyError:
            #self.nextChar()
            # Junk the char
            expected = []
            for key in jumpTable.keys():
                expected.append( "'" + key + "'" )
            raise LexError(self.posStr() + ": "
                           "Unrecognised character '" + self.peekChar() + "'\n"
                           "Expected one of: " + ", ".join(expected))
