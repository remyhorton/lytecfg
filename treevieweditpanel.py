##
## treeeditpanel.py - Tree entry editing panel
## (c) Remy Horton, 2009
##
# {{{ Licence
# This file is part of LyteCFG
# SSI.cgi is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SSI.cgi is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LyteCFG.  If not, see <http://www.gnu.org/licenses/>.
# }}}


import wx
from treenode import  *


# I originally had seperate panels for editing different tree entry types,
# but Sizers did not like that setup.

class TreeViewEditPanel(wx.Panel):
    # {{{ Constants
    ValTypes   = ["String","Number","Name"]
    FieldNames = [
        "$HTTP[\"host\"]",
        "$HTTP[\"url\"]",
        "$HTTP[\"useragent\"]",
        "$HTTP[\"referer\"]",
        "$HTTP[\"querystring\"]",
        "$HTTP[\"remoteip\"]",
        "$HTTP[\"request-method\"]",
        "$HTTP[\"scheme\"]",
        "$HTTP[\"language\"]",
        "$HTTP[\"cookie\"]",
        "$SERVER[\"socket\"]",
        "$PHYSICAL[\"path\"]",
        "$PHYSICAL[\"existing-pth\"]"
        ]
    CondTypes = ["=~","==","!~","!="]
    # }}}
    # {{{ Control id numbers
    TreeEditName=251
    TreeEditType=252
    TreeEditValue=253
    TreeEditValType=254
    TreeEditField=255
    # }}}
    
    def __init__(self,parent):
        # {{{
        wx.Panel.__init__(self,parent,wx.ID_ANY)
        self.treectrl = parent.treectrl
        self.box = wx.StaticBox(self,-1)
        self.field = wx.Choice(self,self.TreeEditField,(-1,-1),(-1,-1),
                               self.FieldNames)
        self.name  = wx.TextCtrl(self,self.TreeEditName,"")
        self.type    = wx.Choice(self,self.TreeEditType)
        self.valType = wx.Choice(self,self.TreeEditValType)     
        self.value = wx.TextCtrl(self,self.TreeEditValue,"")
        
        self.layout = wx.StaticBoxSizer( self.box, wx.VERTICAL )
        self.layout.Add( self.field,0, wx.EXPAND | wx.ALL, 2)
        self.layout.Add( self.name, 0, wx.EXPAND | wx.ALL, 2)
        self.layout.Add( self.type, 0, wx.ALL, 2)
        self.layout.Add( self.valType, 0, wx.ALL, 2)
        self.layout.Add( self.value, 0, wx.EXPAND| wx.ALL, 2)
        self.SetSizer( self.layout )
        self.layout.SetSizeHints ( self )
        self.shownTreeItem = None
        # FIXME: Why doesn't Append( ["=","+="] ) work???
        for i in self.ValTypes:
            self.valType.Append( i )
        self.displayTreeItem(None)

        wx.EVT_CHOICE(self,self.TreeEditField,  self.evtTreeItemEditField)
        wx.EVT_TEXT  (self,self.TreeEditName,   self.evtTreeItemEditName)
        wx.EVT_CHOICE(self,self.TreeEditType,   self.evtTreeItemEditType)
        wx.EVT_TEXT  (self,self.TreeEditValue,  self.evtTreeItemEditValue)
        wx.EVT_CHOICE(self,self.TreeEditValType,self.evtTreeItemEditValType)
        # }}}

    def evtTreeItemEditField(self,event):
        # {{{
        treeItemId = self.currentTreeItemId
        treeItem   = self.treectrl.GetPyData(treeItemId)
        treeItem.field = self.field.GetStringSelection()
        self.treectrl.SetItemText(treeItemId,str(treeItem))
        # }}}
    
    def evtTreeItemEditName(self,event):
        # {{{
        treeItemId = self.currentTreeItemId
        treeItem   = self.treectrl.GetPyData(treeItemId)
        treeItem.name = self.name.GetValue()
        self.treectrl.SetItemText(treeItemId,str(treeItem))
        # }}}

    def evtTreeItemEditType(self,event):
        # {{{
        treeItemId = self.currentTreeItemId
        treeItem   = self.treectrl.GetPyData(treeItemId)
        treeItem.type = self.type.GetStringSelection()
        self.treectrl.SetItemText(treeItemId,str(treeItem))
        # }}}

    def revertChangeToNumber(self,treeItemType):
        # {{{
        self.valType.SetSelection( { TreeStr:0, TreeName:2 } [treeItemType] )
        msg = "Can only change type to Number if there are\n" \
              "only digits in the value"
        errorMsg = wx.MessageDialog(self,msg,"Error", wx.OK)
        errorMsg.ShowModal()
        errorMsg.Destroy()
        # }}}
        
    def evtTreeItemEditValType(self,event):
        # {{{
        treeItemId = self.currentTreeItemId
        treeItem   = self.treectrl.GetPyData(treeItemId)
        itemType = treeItem.__class__

        if itemType == TreeNum or itemType == TreeStr or itemType == TreeName:
            # {{{ Editing value container directly
            if treeItem.__class__ == TreeNum:
                prevItemVal = str(treeItem.value)
            else:
                prevItemVal = treeItem.value
            if treeItem.__class__ == TreeStr or treeItem.__class__ == TreeName:
                if self.valType.GetSelection() == 1 and \
                       not prevItemVal.isdigit():
                    self.revertChangeToNumber(itemType)
                    return
            if self.valType.GetSelection() == 0:
                newTreeItem = TreeStr( prevItemVal )
            elif self.valType.GetSelection() == 1:
                newTreeItem = TreeNum( int(prevItemVal) )
            else:
                newTreeItem = TreeName( prevItemVal )
            self.treectrl.SetPyData(treeItemId, newTreeItem)
            self.treectrl.SetItemText(treeItemId,str(newTreeItem))
            # }}}
        else:
            # {{{ Node stores value indirectly
            prevVal = treeItem.value.value
            if self.valType.GetSelection() == 1 and not prevVal.isdigit():
                if treeItem.value.__class__ != TreeNum:
                    self.revertChangeToNumber(treeItem.value.__class__)
                    return
            if self.valType.GetSelection() == 0:
                treeItem.value = TreeStr( str(prevVal) )
            elif self.valType.GetSelection() == 1:
                treeItem.value = TreeNum( int(prevVal) )
            else:
                treeItem.value = TreeName( str(prevVal) ) 
            self.treectrl.SetItemText(treeItemId,str(treeItem))
            # }}}
        # }}}

    def evtTreeItemEditValue(self,event):
        # {{{
        treeItemId = self.currentTreeItemId
        treeItem   = self.treectrl.GetPyData(treeItemId)
        itemType = treeItem.__class__
        if itemType == TreeStr or itemType == TreeNum or itemType == TreeName:
            self.evtTreeItemEditValueParam(event)
        else:
            self.evtTreeItemEditValueIndirect(event)
        # }}}

    def evtTreeItemEditValueParam(self,event):
        # {{{ Editing a value of a TreeStr/TreeNum/TreeName directly
        treeItemId = self.currentTreeItemId
        treeItem   = self.treectrl.GetPyData(treeItemId)
        treeItemType = treeItem.__class__
        newValue = self.value.GetValue()
        if treeItem.__class__ == TreeNum and not newValue.isdigit():
            msg = "Cannot put non-digits into a Number parameter\n" \
                  "Try changing the type to String first."
            errorMsg = wx.MessageDialog(self,msg,"Error", wx.OK)
            errorMsg.ShowModal()
            errorMsg.Destroy()
            self.value.ChangeValue( str(treeItem.value ) )
        else:
            if treeItemType == TreeStr:
                newVal = TreeStr (str(self.value.GetValue()))
            elif treeItemType == TreeNum:
                newVal = TreeNum (int(self.value.GetValue()))
            elif treeItemType == TreeName:
                newVal = TreeName(str(self.value.GetValue()))
            else:
                raise Exception("Unhandled case")
            self.treectrl.SetPyData(treeItemId, newVal)    
            self.treectrl.SetItemText(treeItemId,str(newVal))
        # }}}
        
    def evtTreeItemEditValueIndirect(self,event):
        # {{{ Editing an object that uses a child TreeStr/Num/Name
        treeItemId = self.currentTreeItemId
        treeItem   = self.treectrl.GetPyData(treeItemId)
        treeItemType = treeItem.__class__
        newValue = self.value.GetValue()
        if treeItem.value.__class__ == TreeNum:
            if not newValue.isdigit():
                msg = "Cannot put non-digits into a Number parameter\n" \
                      "Try changing the type to String first."
                errorMsg = wx.MessageDialog(self,msg,"Error", wx.OK)
                errorMsg.ShowModal()
                errorMsg.Destroy()
                self.value.ChangeValue( str(treeItem.value.value ) )
                return
            else:
                treeItem.value.value = int(self.value.GetValue())
        elif treeItem.value.__class__ == TreeName:
            treeItem.value.name = str(self.value.GetValue())
        else:
            treeItem.value.value = str(self.value.GetValue())            
        self.treectrl.SetItemText(treeItemId,str(treeItem))
        # }}}

    def displayTreeItem(self,treeItemId):
        # {{{ 
        if treeItemId == None:
            self.field.Show(0)
            self.name.Show(0)
            self.type.Show(0)
            self.valType.Show(0)
            self.value.Show(0)
            self.box.Show(0)
            return
        else:
            self.box.Show(1)

        treeItem = self.treectrl.GetPyData(treeItemId)
        self.currentTreeItemId = treeItemId

        if treeItem.__class__ ==TreeArray or treeItem.__class__ ==TreeCondVars:
            self.field.Show(0)
            self.name.Show(0)
            self.type.Show(0)
            self.valType.Show(0)
            self.value.Show(0)
            self.box.Show(0)
        elif treeItem.__class__ == TreeOption:
            self.field.Show(0)
            self.name.Show(1)
            self.name.ChangeValue( treeItem.name )
            self.type.Show(1)
            self.type.Clear()
            for i in ["=","+="]:
                self.type.Append( i )
                self.type.SetSelection( ["=","+="].index(treeItem.type) )
                if treeItem.value != None:
                    if treeItem.value.__class__ == TreeStr:
                        self.valType.SetSelection(0)
                        self.value.ChangeValue( treeItem.value.value )
                    elif treeItem.value.__class__ == TreeNum:
                        self.valType.SetSelection(1)
                        self.value.ChangeValue( str(treeItem.value.value) )
                    elif treeItem.value.__class__ == TreeName:
                        self.valType.SetSelection(2)
                        self.value.ChangeValue( treeItem.value.value )
                    else:
                        raise Exception("evtTreeItemSel: Unhandled case")
                    self.valType.Show(1)
                    self.value.Show(1)
                else:
                    self.valType.Show(0)
                    self.value.Show(0)
        elif treeItem.__class__ == TreeStr:
            self.field.Show(0)
            self.name.Show(0)
            self.type.Show(0)
            self.valType.Show(1)
            self.value.Show(1)
            self.valType.SetSelection( 0 )
            self.value.ChangeValue( treeItem.value )
        elif treeItem.__class__ == TreeNum:
            self.field.Show(0)
            self.name.Show(0)
            self.type.Show(0)
            self.valType.Show(1)
            self.value.Show(1)
            self.valType.SetSelection( 1 )
            self.value.ChangeValue( str(treeItem.value) )
        elif treeItem.__class__ == TreeName:
            self.field.Show(0)
            self.name.Show(0)
            self.type.Show(0)
            self.valType.Show(1)
            self.value.Show(1)
            self.valType.SetSelection( 2 )
            self.value.ChangeValue( treeItem.name )
        elif treeItem.__class__ == TreeArrayEntry:
            self.field.Show(0)
            self.name.ChangeValue( treeItem.key )
            self.name.Show(1)
            self.type.Show(0)
            if treeItem.value != None:               
                if treeItem.value.__class__ == TreeStr:
                    self.valType.SetSelection(0)
                    self.value.ChangeValue( treeItem.value.value )
                elif treeItem.value.__class__ == TreeNum:
                    self.valType.SetSelection(1)
                    self.value.ChangeValue(
                        str(treeItem.value.value))
                elif treeItem.value.__class__ == TreeName:
                    self.valType.SetSelection(2)
                    self.value.ChangeValue( TreeItem.value.value )
                else:
                    raise Exception("evtTreeItemSel: Unhandled case")
                self.valType.Show(1)
                self.value.Show(1)                
            else:
                self.valType.Show(0)
                self.value.Show(0)                
        elif treeItem.__class__ == TreeCond:
            self.field.Show(1)
            self.type.Show(1)
            self.type.Clear()
            for i in self.CondTypes:
                self.type.Append( i )
            index = self.FieldNames.index(treeItem.field)
            self.field.SetSelection( index )
            self.type.SetSelection(
                self.CondTypes.index(treeItem.type))
            if treeItem.value != None:               
                if treeItem.value.__class__ == TreeStr:
                    self.valType.SetSelection(0)
                    self.value.ChangeValue( treeItem.value.value )
                elif treeItem.value.__class__ == TreeNum:
                    self.valType.SetSelection(1)
                    self.value.ChangeValue( str(treeItem.value.value) )
                elif treeItem.value.__class__ == TreeName:
                    self.valType.SetSelection(2)
                    self.value.ChangeValue( treeItem.value.name )
                else:
                    raise Exception("evtTreeItemSel: Unhandled case")
                self.value.Show(1)
                self.valType.Show(1)
            else:
                self.value.ChangeValue( "" )
                self.value.Show(0)
                self.valType.Show(0)
            self.Show(1)
        elif treeItem.__class__ == TreeArray:
            pass
        elif treeItem.__class__ == TreeCondVars:
            pass
        else:
            print "evtTreeItemSel: No handler for " + str(treeItem.__class__)
        self.layout.Layout()
        # }}}
