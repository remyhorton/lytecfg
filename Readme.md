LyteCFG: Lighttpd configuration GUI
===================================
LyteCFG is a GUI for editing of [Lighttpd][lighttpd] configuration files, can
also be used to tidy up functioning configuration files and to track down errors
in broken configuration files. It is written in Python and uses wxWidgets
(formerly wxWindows).

## Current status
LyteCFG is no longer maintained, and this repository conversion of the original
source tarball is for historical reference only. There was a C++ version in the
works, but I have not been able to locate the source code for it.

## Screeenshots

![Shot](basicsSmall.png)
![Shot](mimetypeSmall.png)
![Shot](treeviewSmall.png)

From left-to-right (links to full-size images): [Basic view](basics.png),
[MIME Types](mimetype.png), and [Tree view](treeview.png).

## Licence
IconSharp is licenced under [version 3 of the GPL][gpl3].

## Contact
Send emails to ``remy.horton`` (at) ``gmail.com``

[lighttpd]: href="http://www.lighttpd.net/
[gpl3]: https://www.gnu.org/licenses/gpl.html

