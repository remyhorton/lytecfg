##
## treeviewpanel.py - 
##
##
# {{{ Licence
# This file is part of LyteCFG
# SSI.cgi is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SSI.cgi is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LyteCFG.  If not, see <http://www.gnu.org/licenses/>.
# }}}


import wx
from treenode import  *
from treevieweditpanel import *


class TreeViewPanel(wx.Panel):
    # {{{ Control Id numbers
    TreeCtrl=201
    TreeMenu=210
    TreeAddArrayEntry=210
    TreeDelArrayEntry=211
    TreeAddArray=212
    TreeAddCond=213
    TreeAddElseCond=214
    TreeAddOption=215
    TreeAddMerge=216
    TreeAddNum=217
    TreeAddStr=218
    TreeAddName=219
    TreeDelete=220
    TreeInsCond=221
    TreeInsOption=222
    TreeInsMerge=223
    TreeEdited=250
    TreeEditName=251
    TreeEditType=252
    TreeEditValue=253
    TreeEditValType=254
    # }}}

    def __init__(self,rootWindow,parent):
        # {{{
        wx.Panel.__init__(self,parent)
        self.rootWindow = rootWindow
        self.treectrl = wx.TreeCtrl(self, self.TreeCtrl, (0,0),
                                    wx.DefaultSize,
                                    wx.TR_HAS_BUTTONS   |
                                    wx.TR_HIDE_ROOT     |
                                    wx.TR_LINES_AT_ROOT
                                    )
        wx.EVT_TREE_SEL_CHANGED(self,self.TreeCtrl,self.evtTreeItemSel)
        wx.EVT_TREE_ITEM_MENU  (self,self.TreeCtrl,self.evtTreeItemMenu)

        # {{{ TreeCtrl popup menus
        self.treeMenus = dict()
        
        # {{{ Conditional
        menu = wx.Menu()
        menu.Append(self.TreeAddCond,     "Insert conditional")      
        menu.Append(self.TreeAddOption,   "Insert option directive")
        menu.Append(self.TreeAddMerge,    "Insert merge directive")
        menu.AppendSeparator()
        menu.Append(self.TreeAddNum,      "Attach number")
        menu.Append(self.TreeAddStr,      "Attach string")
        menu.Append(self.TreeAddName,     "Attach variable")
        menu.Append(self.TreeAddArray,    "Attach array")        
        menu.Append(self.TreeInsCond,     "Attach conditional")
        menu.Append(self.TreeInsOption,   "Attach option directive")        
        menu.Append(self.TreeInsMerge,    "Attach merge directive")
        menu.Append(self.TreeAddElseCond, "Append 'else' conditional")  
        menu.AppendSeparator()
        menu.Append(self.TreeDelete,      "Delete")
        self.treeMenus[TreeCond] = menu
        # }}}
        # {{{ Condition variables
        menu = wx.Menu()
        menu.Append(self.TreeAddNum,      "Add number")
        menu.Append(self.TreeAddStr,      "Add string")
        menu.Append(self.TreeAddName,     "Add variable")
        menu.AppendSeparator()
        menu.Append(self.TreeDelete,      "Delete")
        self.treeMenus[TreeCondVars] = menu
        # }}}
        # {{{ Option (or Merge) directive
        menu = wx.Menu()
        menu.Append(self.TreeAddCond,     "Insert conditional")    
        menu.Append(self.TreeAddOption,   "Insert option directive")
        menu.Append(self.TreeAddMerge,    "Insert merge directive")
        menu.AppendSeparator()
        menu.Append(self.TreeAddNum,      "Attach number")
        menu.Append(self.TreeAddStr,      "Attach string")
        menu.Append(self.TreeAddName,     "Attach variable")
        menu.Append(self.TreeAddArray,    "Attach array")        
        menu.AppendSeparator()
        menu.Append(self.TreeDelete,      "Delete")
        self.treeMenus[TreeOption] = menu
        # }}}
        # {{{ Array root
        menu = wx.Menu()
        menu.Append(self.TreeAddArrayEntry, "Add array entry")
        menu.AppendSeparator()
        menu.Append(self.TreeAddNum,      "Attach number")
        menu.Append(self.TreeAddStr,      "Attach string")
        menu.Append(self.TreeAddName,     "Attach variable")
        menu.Append(self.TreeAddArray,    "Attach array") 
        menu.AppendSeparator()
        menu.Append(self.TreeDelete,        "Delete array")
        self.treeMenus[TreeArray] = menu
        # }}}
        # {{{ Array entry 
        menu = wx.Menu()
        menu.Append(self.TreeAddArrayEntry, "Insert array entry")
        menu.AppendSeparator()
        menu.Append(self.TreeAddNum,      "Attach number")
        menu.Append(self.TreeAddStr,      "Attach string")
        menu.Append(self.TreeAddName,     "Attach variable")
        menu.Append(self.TreeAddArray,    "Attach array") 
        menu.AppendSeparator()
        menu.Append(self.TreeDelete,        "Delete array entry")
        self.treeMenus[TreeArrayEntry] = menu
        # }}}
        # {{{ Parameter entries
        menu = wx.Menu()
        menu.Append(self.TreeAddNum,        "Insert number")
        menu.Append(self.TreeAddStr,        "Insert string")
        menu.Append(self.TreeAddName,       "Insert variable")
        menu.Append(self.TreeAddArray,      "Insert array")
        menu.AppendSeparator()
        menu.Append(self.TreeDelete,        "Delete")
        self.treeMenus[TreeNum] = menu
        self.treeMenus[TreeStr] = menu
        self.treeMenus[TreeName] = menu
        # }}}
        # {{{ Assign menu event hooks
        wx.EVT_MENU(self,self.TreeAddArrayEntry,self.evtTreeAddArrayEntry)
        wx.EVT_MENU(self,self.TreeAddArray,self.evtTreeAddParam)
        wx.EVT_MENU(self,self.TreeAddCond,self.evtTreeAddCond)
        wx.EVT_MENU(self,self.TreeAddElseCond,self.evtTreeAddElseCond)
        wx.EVT_MENU(self,self.TreeAddOption,self.evtTreeAddOption)
        wx.EVT_MENU(self,self.TreeAddMerge,self.evtTreeAddOption)
        wx.EVT_MENU(self,self.TreeAddNum, self.evtTreeAddParam)
        wx.EVT_MENU(self,self.TreeAddStr, self.evtTreeAddParam)
        wx.EVT_MENU(self,self.TreeAddName,self.evtTreeAddParam)
        wx.EVT_MENU(self,self.TreeInsOption,self.evtTreeInsOption) 
        wx.EVT_MENU(self,self.TreeInsMerge,self.evtTreeInsOption)
        wx.EVT_MENU(self,self.TreeInsCond, self.evtTreeInsCond)        
        wx.EVT_MENU(self,self.TreeDelete,self.evtTreeDelete)
        # }}}

        # }}} (end of TreeCtrl popup menus)

        self.editArea = TreeViewEditPanel(self)
        sizer = wx.BoxSizer( wx.HORIZONTAL )
        sizer.Add(self.treectrl,1,wx.EXPAND,0)
        sizer.Add(self.editArea,1,wx.EXPAND|wx.ALL,4)
        sizer.SetSizeHints( self )
        self.SetSizer( sizer )      
        # }}}

    def evtTreeAddArrayEntry(self,event):
        # {{{
        treeItemId = self.menuEventNodeId
        treeItem   = self.treectrl.GetPyData(treeItemId)
        treeItemParentId = self.treectrl.GetItemParent(treeItemId)
        treeItemParent   = self.treectrl.GetPyData(treeItemParentId)
        node = TreeArrayEntry("key",TreeStr("value"))
        if treeItem.__class__ == TreeArrayEntry:             
            self.treectrl.InsertItem(treeItemParentId,treeItemId,
                                     str(node),-1,-1,wx.TreeItemData(node))
        elif treeItem.__class__ == TreeArray:
            self.treectrl.InsertItemBefore(treeItemId,0,str(node),-1,-1,
                                           wx.TreeItemData(node))
            self.treectrl.Expand(treeItemId)
        else:
            raise Exception("Unhandled case")
        # }}}
            
    def evtTreeAddCond(self,event):
        # {{{
        treeItemId = self.menuEventNodeId
        treeItem   = self.treectrl.GetPyData(treeItemId)
        treeItemParentId = self.treectrl.GetItemParent(treeItemId)       
        node = TreeCond("$HTTP[\"host\"]","==",TreeStr("www.example.com"))
        if treeItem.__class__ == TreeOption or \
           treeItem.__class__ == TreeCond:
            newNode = self.treectrl.InsertItem(treeItemParentId,treeItemId,
                                               str(node),
                                               -1,-1,wx.TreeItemData(node))
            self.treectrl.SelectItem(newNode)
        else:
            raise Exception("Unhandled case: "+ str(treeItem.__class__))

        # }}}

    def evtTreeInsCond(self,event):
        # {{{
        treeItemId = self.menuEventNodeId
        treeItem   = self.treectrl.GetPyData(treeItemId)
        treeItemParentId = self.treectrl.GetItemParent(treeItemId)
        node = TreeCond("$HTTP[\"host\"]","==",TreeStr("www.example.com"))
        if treeItem.value == None:
            (vals,junk) = self.treectrl.GetFirstChild( treeItemId )
            self.treectrl.SelectItem(
                self.treectrl.InsertItem(treeItemId,vals,str(node),
                                         -1,-1,wx.TreeItemData(node)))
        else:
            self.treectrl.SelectItem(
                self.treectrl.PrependItem(treeItemId,str(node),
                                          -1,-1,wx.TreeItemData(node)))
        # }}}

    def evtTreeAddElseCond(self,event):
        #print "GetInt: " + str(event.GetInt())
        #print "GetSel: " + str(event.GetSelection())
        #print "GeClD: " + str(event.GetClientData())
        #print "GetID: " + str(event.GetId())
        #print "EvtObj: " + str(event.GetEventObject())
        #print "EvtType: " + str(event.GetEventType())
        #print "Item: " + str(self.menuEventNodeId)
        self.rootWindow.notImpl("AddElseCond")
        
    def evtTreeAddOption(self,event):
        # {{{
        treeItemId = self.menuEventNodeId
        treeItem   = self.treectrl.GetPyData(treeItemId)
        treeItemParentId = self.treectrl.GetItemParent(treeItemId)
        optionType = {
            self.TreeAddOption : "=", self.TreeAddMerge : "+="
            }[event.GetId()]
        node = TreeOption("new",optionType,TreeStr(""))
        if treeItem.__class__ == TreeOption or \
           treeItem.__class__ == TreeCond:
            self.treectrl.InsertItem(treeItemParentId,treeItemId,str(node),
                                     -1,-1,wx.TreeItemData(node))
        else:
            raise Exception("Unhandled case: "+ str(treeItem.__class__))

        # }}}

    def evtTreeInsOption(self,event):
        # {{{
        treeItemId = self.menuEventNodeId
        treeItem   = self.treectrl.GetPyData(treeItemId)
        treeItemParentId = self.treectrl.GetItemParent(treeItemId)
        optionType = {
            self.TreeInsOption : "=", self.TreeInsMerge : "+="
            }[event.GetId()]
        node = TreeOption("new",optionType,TreeStr(""))
        if treeItem.value == None:
            (vals,junk) = self.treectrl.GetFirstChild( treeItemId )
            self.treectrl.SelectItem(
                self.treectrl.InsertItem(treeItemId,vals,str(node),
                                         -1,-1,wx.TreeItemData(node)))
        else:
            self.treectrl.SelectItem(
                self.treectrl.PrependItem(treeItemId,str(node),
                                          -1,-1,wx.TreeItemData(node)))
        # }}}

    def evtTreeDelete(self,event):
        # {{{
        treeItemId = self.menuEventNodeId
        treeItem   = self.treectrl.GetPyData(treeItemId)
        treeItemParentId = self.treectrl.GetItemParent(treeItemId)
        treeItemParent   = self.treectrl.GetPyData(treeItemParentId)
        entryType = treeItem.__class__
        if \
               entryType == TreeOption or \
               entryType == TreeArrayEntry:
            self.treectrl.Delete(treeItemId)
            
        elif entryType == TreeCond:
            self.treectrl.Delete(treeItemId)
            
        elif entryType == TreeCondVars:
            self.treectrl.Delete(treeItemId)
            treeItemParent.value = TreeStr("")
            self.treectrl.SetItemText(treeItemParentId,str(treeItemParent))

        elif entryType == TreeStr     or \
               entryType == TreeNum   or \
               entryType == TreeName  or \
               entryType == TreeArray:
            self.treectrl.Delete(treeItemId)           
            if self.treectrl.GetChildrenCount(treeItemParentId) == 1:
                # {{{ If only one non-array child remains, combine with parent
                (treeItemId,junk) = \
                                  self.treectrl.GetFirstChild(treeItemParentId)
                treeItem   = self.treectrl.GetPyData(treeItemId)
                if treeItem.__class__ != TreeArray:
                    if treeItemParent.__class__ != TreeCondVars:    
                        treeItemParent.value = treeItem
                        self.treectrl.Delete(treeItemId)
                        self.treectrl.SetItemText(treeItemParentId,
                                                  str(treeItemParent))
                    else:
                        treeAncestorId = self.treectrl.GetItemParent(
                            treeItemParentId)
                        treeAncestor = self.treectrl.GetPyData(treeAncestorId)
                        treeAncestor.value = treeItem
                        self.treectrl.Delete(treeItemParentId)
                        self.treectrl.SetItemText(treeAncestorId,
                                                  str(treeAncestor))
                # }}}
            elif self.treectrl.GetChildrenCount(treeItemParentId) == 0 and \
                 treeItemParent.__class__ == TreeCondVars:
                # {{{ Delete CondVar placeholder if it is empty
                treeAncestorId = self.treectrl.GetItemParent(
                    treeItemParentId)
                treeAncestor = self.treectrl.GetPyData(treeAncestorId)
                treeAncestor.value = TreeStr("")
                self.treectrl.Delete(treeItemParentId)
                self.treectrl.SetItemText(treeAncestorId,str(treeAncestor))
                # }}}
            elif self.treectrl.GetChildrenCount(treeItemParentId) == 0:
                treeItemParent.value = TreeStr("")
                self.treectrl.SetItemText(treeItemParentId,str(treeItemParent))
            else:
                raise Exception("Not handled: " + str(entryType))
        else:
            raise Exception("Not handled: " + str(entryType))
        # }}}

    def evtTreeAddParam(self,event):
        # {{{
        treeItemId = self.menuEventNodeId
        treeItem   = self.treectrl.GetPyData(treeItemId)
        node = {
            self.TreeAddNum   : TreeNum(0),
            self.TreeAddStr   : TreeStr(""),
            self.TreeAddName  : TreeName(""),
            self.TreeAddArray : TreeArray()
            }[event.GetId()]
        if treeItem.__class__ == TreeOption or \
               treeItem.__class__ == TreeArrayEntry:
            # {{{ Option directive
            if treeItem.value != None:
                # Move existing from the 'single' slot to a child node
                self.treectrl.AppendItem(treeItemId, str(treeItem.value),-1,-1,
                                         wx.TreeItemData(treeItem.value))
                treeItem.value = None
                newNode = self.treectrl.AppendItem(treeItemId, str(node),-1,-1,
                                         wx.TreeItemData(node))
                self.treectrl.SetItemText(treeItemId,str(treeItem))
                self.treectrl.SelectItem(newNode)
            else:
                newNode= self.treectrl.InsertItemBefore(treeItemId,0,
                                                        str(node),-1,-1,
                                                        wx.TreeItemData(node))
            self.treectrl.Expand(treeItemId)
            self.treectrl.SelectItem(newNode)
            # }}}
        elif treeItem.__class__ == TreeNum  or \
             treeItem.__class__ == TreeStr  or \
             treeItem.__class__ == TreeName or \
             treeItem.__class__ == TreeArray:
            # {{{
            treeItemParentId = self.treectrl.GetItemParent(treeItemId)
            treeItemParent   = self.treectrl.GetPyData(treeItemParentId)
            newNode = self.treectrl.InsertItem(treeItemParentId,treeItemId,
                                               str(node),-1,-1,
                                               wx.TreeItemData(node))
            self.treectrl.SelectItem(newNode)
            # }}}
            
        elif treeItem.__class__ == TreeCond:
            if treeItem.value != None:
                vals = \
                     self.treectrl.PrependItem(treeItemId,
                                               "Condition variables",-1,-1,
                                               wx.TreeItemData(TreeCondVars()))
                self.treectrl.AppendItem(vals,str(treeItem.value),-1,-1,
                                wx.TreeItemData(treeItem.value))
                self.treectrl.Expand(treeItemId)
                self.treectrl.Expand(vals)
                treeItem.value = None
                self.treectrl.SetItemText(treeItemId,str(treeItem))
            else:
                (vals,junk) = self.treectrl.GetFirstChild( treeItemId )
            self.treectrl.SelectItem(
                self.treectrl.PrependItem(vals,str(node),-1,-1,
                                          wx.TreeItemData(node)))
        elif treeItem.__class__ == TreeCondVars:
            self.treectrl.SelectItem(
                self.treectrl.PrependItem(treeItemId,str(node),-1,-1,
                                          wx.TreeItemData(node)))
        else:
            raise Exception("Unhandled case: "+ str(treeItem.__class__))

        # }}}

    def evtTreeItemMenu(self,event):
        # {{{
        itemid = event.GetItem()
        item = self.treectrl.GetPyData(itemid)
        self.menuEventNodeId = itemid
        self.PopupMenu( self.treeMenus[item.__class__],event.GetPoint())
        # }}}

    def evtTreeItemSel(self,event):
        # {{{ 
        itemid = event.GetItem()
        self.editArea.displayTreeItem( itemid )       
        # }}}


        
    def getTreeText(self):
        # {{{ Used for saving
        rootId = self.treectrl.GetRootItem()
        (nodeId,cookie) = self.treectrl.GetFirstChild(rootId)
        treeStrs = []
        while nodeId.IsOk():
            treeStrs.extend( self.traverseTree(nodeId) )
            treeStrs.append("\n")
            (nodeId,cookie) = self.treectrl.GetNextChild(rootId,cookie)
        return " ".join(treeStrs)
        # }}}
        
    def traverseChilds(self,rootId,seperator):
        # {{{
        (nodeId,cookie) = self.treectrl.GetFirstChild(rootId)
        if nodeId.IsOk():
            treeStrs = []
            treeStrs.extend(  self.traverseTree(nodeId) )
            (nodeId,cookie) = self.treectrl.GetNextChild(rootId,cookie)
            while nodeId.IsOk():
                treeStrs.extend( seperator )
                treeStrs.extend(  self.traverseTree(nodeId) )
                (nodeId,cookie) = self.treectrl.GetNextChild(rootId,cookie)
            return treeStrs
        else:
            return []
        # }}}

    def traverseTree(self,rootId):
        # {{{
        root = self.treectrl.GetPyData(rootId)
        rootType = root.__class__
        treeStrs = []

        if rootType == None:
            return []
        elif rootType == TreeOption:
            treeStrs.append( "%s %s " % (root.name,root.type) )
            if root.value != None:
                treeStrs.append( str(root.value) )
            treeStrs.extend( self.traverseChilds(rootId,"+") )
            
        elif rootType == TreeArray:
            treeStrs.extend( "(" )
            treeStrs.extend( self.traverseChilds(rootId,", ") )
            treeStrs.extend( ")" )
            
        elif rootType == TreeArrayEntry:
            if len(root.key) > 0:
                treeStrs.append( "\"" + root.key + "\" => " )
            if root.value != None:
                treeStrs.append( str(root.value) )
            treeStrs.extend( self.traverseChilds(rootId,"+") )
        elif rootType == TreeNum:
            return [str(root)]
        elif rootType == TreeName:
            return [str(root)]
        elif rootType == TreeStr:
            return [str(root)]
        elif rootType == TreeCond:
            # {{{ TreeCond is an akward case
            treeStrs.append( "%s %s " % (root.field,root.type) )
            if root.value != None:
                treeStrs.append( str(root.value) )
                treeStrs.extend( "{\n" )
                treeStrs.extend( self.traverseChilds(rootId,"\n") )
                treeStrs.extend( "}\n" )
            else:
                (nodeId,cookie) = self.treectrl.GetFirstChild(rootId)
                treeStrs.extend( self.traverseChilds(nodeId,"+") )
                treeStrs.extend( "{\n" )
                
                (nodeId,cookie) = self.treectrl.GetNextChild(rootId,cookie)
                if nodeId.IsOk():
                    treeStrs.extend(  self.traverseTree(nodeId) )
                    (nodeId,cookie) = self.treectrl.GetNextChild(rootId,cookie)
                while nodeId.IsOk():
                    treeStrs.extend( "+" )
                    treeStrs.extend(  self.traverseTree(nodeId) )
                    (nodeId,cookie) = self.treectrl.GetNextChild(rootId,cookie)
                treeStrs.extend( "}\n" )                
            # }}}
        elif rootType == TreeCondVars:
            return self.traverseChilds(rootId,"+")
        else:
            raise Exception("Unhandled case: " + str(rootType))
        return treeStrs
        # }}}

        


