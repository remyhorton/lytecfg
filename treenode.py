##
## treenode.py - TreeCtrl node data objects
## (c) Remy Horton, 2009
##
# {{{ Licence
# This file is part of LyteCFG
# SSI.cgi is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SSI.cgi is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LyteCFG.  If not, see <http://www.gnu.org/licenses/>.
# }}}


# These classes are used to identify what node type an entry within the
# TreeCtrl is, and to hold data related to the entry.
#
# For some reason although .AppendItem happily accepts sub-classes of
# wx.TreeItemData, i could not work out how to get at the data when
# servicing wx.TreeCtrl events. I ended up instead wrapping these
# classes manually before passing them to .AppendItem and using
# GetPyData(). I suspect the problem is due to marshalling between
# Python and the underlying C++ based wxWidgets code.


class TreeNode:
    def __init__(self,value):
        self.value = value
    def __str__(self):
        return str(self.value)

class TreeOption(TreeNode):
    def __init__(self,name,type,value):
        self.name = name
        self.type = type
        self.value = value
    def __str__(self):
        if self.value != None:
            return self.name +" "+ self.type +" "+ str(self.value)
        else:
            return "%s %s ..." % (self.name,self.type)

class TreeNum(TreeNode):
    pass

class TreeStr(TreeNode):
    def __str__(self):
        return "\""+self.value+"\""

class TreeName(TreeNode):
    pass

class TreeArray(TreeNode):
    def __init__(self):
        self.value = None
    def __str__(self):
        return "Array"

class TreeArrayEntry(TreeNode):
    def __init__(self,key,value):
        self.key = key
        self.value = value
    def __str__(self):
        if self.value != None:
            return self.key + " => " + str(self.value)
        else:
            return self.key + " => ..."

class TreeCond(TreeNode):
    def __init__(self,field,cond,value):
        self.field = field
        self.type = cond
        self.value = value
    def __str__(self):
        if self.value != None:
            return "%s %s %s" % (self.field,self.type,self.value)
        else:
            return "%s %s ..." % (self.field,self.type)

class TreeCondVars(TreeNode):
    def __init__(self):
        self.value = None
    def __str__(self):
        return "TreeCondVars"
