LyteCFG v0.5a
=============

LyteCFG is a configuration GUI for Lighttpd. It is currently in alpha, so there
is no guarantee that the config files it generated actually work. There are also 
known bugs, so this is more of a preview release.


Program structure
-----------------
The program falls into roughly three parts, and so do my plans:

* File loading
This is implemented as a recursive-descent parser. My plan is to rewrite this 
so that it is able to (partly) recover configurations from malformatted files.

* Tree view
Internally the config file is stored in a TreeCtrl, and this can be edited 
directly. This is currently implemented, though it is really intended as 
the 'advanced' view. Only expected change to this section, apart from fixing
any bugs that crop up, is scemantic checking. I am convinced some of the things
the Lighttpd config file syntax allows, such as $HTTP["host"]==("a"=>"b"), 
arn't actually valid..

* Alternative views
Longer term my plan is to add alternative views of the configutation file, as 
the TreeView does assume some knowledge of the Lighttpd file format. 
Not yet decided what form this will take, but will probably include different
presentation of module confgiurations (eg SSL and Vhosts), and wizards for
creation of some more basic config files.


To-do List
----------
Outright broken:
* Support for include
* Support for else conditionals (ie 'else $HTTP["host"]')

Needs addressing:
* Handling of '$HTTP["host"] == var.name "string"' is an ugly hack
* Scemantic checking



Using
-----
Run lytecfg.py


Dependencies
------------
LiteCFG is written using Python v2.5.2 and wxPython 2.8.8.x. I have yet to 
do any serious investigation into which versions LyteCFG will work with.


Documentation
-------------
This will be available at http://lytecfg.remynet.org once it has been written..


Licence
-------
SSI.cgi is currently licenced under v3 of the GPL (see Licence.txt). Had 
considered the BSD licence, but I am yet to be convonced of its advantage 
or need.


Contact
-------
If i think this program is actually useful to someone, i'm more likley to
polish off any rough edges people find.

Website: http://lytecfg.remynet.org
  Email: remy.horton (at) bris.ac.uk
