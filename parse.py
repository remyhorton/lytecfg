##
## parse.py - Token parsing
## (c) Remy Horton, 2009
##
# {{{ Licence
# This file is part of LyteCFG
# SSI.cgi is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SSI.cgi is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LyteCFG.  If not, see <http://www.gnu.org/licenses/>.
# }}}



from lex  import Token, Lex
from node import *


# Parse Error exception
class ParseError(Exception):
    def __init__(self,expected,foundToken):
        self.expected = expected
        self.found = foundToken
    def __str__(self):
        return "Parse Error (%i,%i): Expected %s\nFound '%s'" % \
               (self.found.line, self.found.column, \
               self.expected, self.found.value)



# Recursive-descent parser. At the moment an error in the config
# file causes the parser to bail out.

class Parse:
    # Initaliser
    def __init__(self,tokenSource):
        self.lexer = tokenSource
        self.peekedToken = None


    # Get next token. There is a 1-token buffer to allow peeking.
    # The tokeniser keeps comments, but for now they are thrown away
    # here rather than attempting to associate them with Option/Merge
    # directives. 
    def nextToken(self):
        if self.peekedToken == None:
            while True:
                token = self.lexer.getToken();
                if token.tokType != Token.Comment:
                    return token
        else:
            token = self.peekedToken
            self.peekedToken = None;
            return token


    # Peek at next token, without consuming it.
    def peekToken(self):
        if self.peekedToken == None:
            self.peekedToken = self.nextToken()
        return self.peekedToken


    # Entrypoint
    def parseTokensUNUSED(self):
        try:
            cfgFile = self.parseConfigFile()
            return cfgFile
#            for cfg in cfgFile:
#                cfg.traverse(0)
        except ParseError,  ex:
            print (str(ex))


    #
    # The self.parse*() functions (remainder of this file) do the
    # actual parsing. The 'current' token is either passed forward
    # or discarded, depending on whether the called function requires
    # the associated value. peekToken() is used for the few cases where
    # it is not known in advance whether a token can be consumed (an
    # alternative is to back-track, but that is not implemented).
    #
    def parseConfigFile(self):
        cfgFile = []
        while True:
            token = self.nextToken();
            if token.tokType != Token.EOF:
                cfgFile.append( self.parseConfigEntry(token) )
            else:
                break
        return cfgFile


    def parseConfigEntry(self,lastToken):
        try:
            node = {
                Token.Var    : self.parseOption,
                Token.Field  : self.parseCond
                }[lastToken.tokType](lastToken)
        except KeyError:
            raise ParseError("option or condition name",token);
        # Ideally ought to catch ParseError here as well, and
        # make an attempt at getting the parser back on track.
        return node


    def parseOption(self,lastToken):
        name = lastToken.value
        token = self.nextToken()
        if lastToken.value == "include":
            if token.tokType != Token.String:
                raise ParseError("\"String\"",token)
            return IncludeNode(token.value)
        else:
            if token.tokType != Token.Assign and token.tokType != Token.PlusEq:
                raise ParseError("= or +=",token)
            value = self.parseValue(self.nextToken())
            if token.tokType == Token.Assign:
                return AssignNode(name,value)
            else:
                return AppendNode(name,value)


    def parseValue(self,lastToken):
        values = []
        try:
            node = {
                Token.String : lambda t : StringNode(t.value),
                Token.Num    : lambda t : NumberNode(t.value),
                Token.Var    : lambda t : NameNode(t.value),
                Token.OpenPa : lambda t : self.parseArray()
            }[lastToken.tokType](lastToken)
        except KeyError:
            raise ParseError("\", Number, 'enable', 'disable', or '('",\
                             lastToken)
        else: # attached to try
            values.append(node);
            if self.peekToken().tokType == Token.Plus:
                self.nextToken()
                values.extend(  self.parseValue(self.nextToken())  )
            return values


    def parseArray(self):
        arrayEntries = []
        while True:
            arrayEntries.append( self.parseArrayEntry() )
            token = self.nextToken()
            if token.tokType == Token.ClosePa:
                break
            elif token.tokType == Token.Comma:
                if self.peekToken().tokType == Token.ClosePa:
                    print ("WARNING: ',' at end of array")
                    self.nextToken();
                    break
                continue
            else:
                raise ParseError("',' or ')'",token)
        return ArrayNode(arrayEntries)
    
        
    def parseArrayEntry(self):
        token = self.nextToken()
        key = ""
        if self.peekToken().tokType == Token.Arrow:
            if token.tokType != Token.String:
                raise ParseError("String (array key) before '=>'",token)
            key = token.value
            self.nextToken()           
            token = self.nextToken()
        ###print "***" + str(token)
        value = self.parseValue(token)
        ###print ">>>" + str(self.peekToken())        
        return (key,value)
            

    def parseCond(self,lastToken):
        fieldName = lastToken.value
        token = self.nextToken()
        try:
            condition = {
                Token.IsExp : "~=",
                Token.NoExp : "!~",
                Token.Eq    : "=="
                }[token.tokType]
        except KeyError:
            raise ParseError("'~=', '==', or '!~'",token)
        value = self.parseValue(self.nextToken())
        token = self.nextToken()
        payload = []
        if token.tokType != Token.OpenBr:
            raise ParseError("'{'",token)
        while True:
            token = self.nextToken()
            if token.tokType == Token.CloseBr:
                break
            payload.append( self.parseConfigEntry(token) )
        return CondNode(fieldName,condition,value,payload)

