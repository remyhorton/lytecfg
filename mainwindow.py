##
## mainwindow.py - Main program window
## (c) Remy Horton, 2009
##
# {{{ Licence
# This file is part of LyteCFG
# SSI.cgi is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# SSI.cgi is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with LyteCFG.  If not, see <http://www.gnu.org/licenses/>.
# }}}



from lex      import Token,Lex,LexError
from parse    import Parse,ParseError
from node     import *
from treenode import *
from treeviewpanel import *

from gui import *






class MainWindow(wx.Frame):
    MenuExit=101
    MenuHelp=102
    MenuOpen=103
    MenuSave=104
    #TreeCtrl=201
    #TreeMenu=210
    TabsCtrl=301
    def __init__(self):
        # {{{
        self.app = wx.PySimpleApp()
        style = wx.DEFAULT_FRAME_STYLE - (wx.RESIZE_BORDER|wx.MAXIMIZE_BOX)
        wx.Frame.__init__(self,None,-1, "LyteCFG", (-1,-1), (620,450), style)
        
        # {{{ Notebook ctrl pages
        self.tabs = wx.Choicebook(self,self.TabsCtrl, (0,0), (-1,-1),
        #self.tabs = wx.Notebook(self,self.TabsCtrl, (0,0), (-1,-1),
                                wx.NB_BOTTOM)        
        self.treeViewPanel = TreeViewPanel(self,self.tabs)
        self.basicCfgPanel = ServerBasicsPanel(self,self.tabs)
        self.dirListsPanel = DirListPanel(self,self.tabs)
        self.virtHostPanel = VirtualHostsPanel(self,self.tabs)
        self.logPanel      = LogPanel(self,self.tabs)        
        self.tabs.AddPage( self.treeViewPanel, "Tree View" )        
        self.tabs.AddPage( self.basicCfgPanel, "Server basics" )
        self.tabs.AddPage( self.dirListsPanel, "Directory listing" )
        self.tabs.AddPage( self.virtHostPanel, "Virtual Hosts" )
        self.tabs.AddPage( self.logPanel,       "Parse log" )        
        wx.EVT_NOTEBOOK_PAGE_CHANGING(self,self.TabsCtrl, self.pageChange )
        wx.EVT_NOTEBOOK_PAGE_CHANGED(self, self.TabsCtrl, self.pageChangeDone )
        # }}}
        
        # {{{ Menus
        menuBar = wx.MenuBar()
        menu = wx.Menu()
        menu.Append(self.MenuOpen, "Open")
        menu.Append(self.MenuSave, "Save as..")
        menu.Append(self.MenuExit, "E&xit")
        menuBar.Append(menu, "&File")
        menu = wx.Menu()
        menu.Append(self.MenuHelp, "A&bout")
        menuBar.Append(menu, "Help")
        self.SetMenuBar(menuBar)
        wx.EVT_MENU(self,self.MenuOpen,self.LoadFile)
        wx.EVT_MENU(self,self.MenuSave,self.SaveFile)        
        wx.EVT_MENU(self,self.MenuExit,self.Quit)
        wx.EVT_MENU(self,self.MenuHelp,self.ShowHelp)
        # }}}

        # Show main window and go into event loop
        self.Show(1)
        self.app.MainLoop()
        # }}}

    def pageChange(self,event):
        pass #print "Tab: %i -> " % (event.GetOldSelection() )

    def pageChangeDone(self,event):
        pass #print "Tab: -> %i " % ( event.GetSelection() )

    def errorDialog(self,title,msg):
        dialog = wx.MessageDialog(self,msg,title,wx.OK|wx.ICON_EXCLAMATION)
        dialog.ShowModal()
        dialog.Destroy()
            
    # Select and loads a file. 
    def LoadFile(self,event):
        # {{{
        flags = wx.FD_OPEN | wx.FD_FILE_MUST_EXIST | wx.FD_CHANGE_DIR 
        fileDialogue = wx.FileDialog(self, "Load config file..", "","",
                                     "Lighttpd config file (*.cfg)|*.cfg"
                                     "|All files|*.*", flags)
        if fileDialogue.ShowModal() == wx.ID_OK:
            # Parse config file
            lexer = Lex()
            parser = Parse(lexer)
            try:
                lexer.openSource(fileDialogue.GetPath());
                tree = parser.parseConfigFile()
            except (LexError,ParseError), e:
                self.errorDialog("Loading error",str(e))
            else:
                # Insert parse tree into tree control
                treectrl = self.treeViewPanel.treectrl;
                treectrl.DeleteAllItems()
                ctrlroot = treectrl.AddRoot("Config")
                for branch in tree:
                    self.convTree(treectrl,ctrlroot,branch)
            # clean up
            del lexer
            del parser
        fileDialogue.Destroy()

        # }}}


    # Really should give each node a proper id number, but by the time
    # you've sorted out a lookup table, you may as well just keep track
    # of the object via previous selection/menu events..
    def convTree(self,ctrl,root,branch):
        # {{{
        if branch.__class__ == list:
            for item in branch:
                self.convTree(ctrl,root,item)
        elif branch.__class__ == StringNode:
            node = TreeStr(branch.value)
            ctrl.AppendItem(root,str(node),-1,-1,wx.TreeItemData(node))
        elif branch.__class__ == NumberNode:
            node = TreeNum(branch.value)
            ctrl.AppendItem(root,str(node),-1,-1,wx.TreeItemData(node))
        elif branch.__class__ == NameNode:
            node = TreeName(branch.value)
            ctrl.AppendItem(root,str(node),-1,-1,wx.TreeItemData(node))
        elif branch.__class__ == ArrayNode:
            arrayNode = ctrl.AppendItem(root,"Array", -1, -1,
                                        wx.TreeItemData(TreeArray()))
            for entry in branch.value:
                (k,v) = entry
                if len(v)==1 and v[0].__class__ == StringNode:
                    node = TreeArrayEntry(k,TreeStr(v[0].value))
                    ctrl.AppendItem(arrayNode,str(node),-1,-1,
                                    wx.TreeItemData(node))
                elif len(v)==1 and v[0].__class__ == NumberNode:
                    node = TreeArrayEntry(k,TreeNum(v[0].value))
                    ctrl.AppendItem(arrayNode,str(node),-1,-1,
                                    wx.TreeItemData(node))
                else:
                    node = TreeArrayEntry(k,None)
                    node = ctrl.AppendItem(arrayNode,str(node),-1,-1,
                                           wx.TreeItemData(node))
                    self.convTree(ctrl,node,v)
        
        elif branch.__class__ == AssignNode or branch.__class__ == AppendNode:
            type = {AssignNode : "=", AppendNode : "+=" }[branch.__class__]
            if len(branch.value) == 1 :
                nodeVal  = branch.value[0].value
                nodeType = branch.value[0].__class__
                if nodeType == NumberNode:
                    node = TreeOption(branch.name,type,TreeNum(nodeVal))
                    ctrl.AppendItem(root,str(node),-1,-1,
                                    wx.TreeItemData(node))
                    return
                if nodeType == StringNode:
                    node = TreeOption(branch.name,type,TreeStr(nodeVal))
                    ctrl.AppendItem(root,str(node),-1,-1,
                                    wx.TreeItemData(node))
                    return
                if nodeType == ArrayNode:
                    node = TreeOption(branch.name,type,None)
                    node = ctrl.AppendItem(root,str(node),-1,-1,
                                           wx.TreeItemData(node))
                    self.convTree(ctrl,node,branch.value)
                    return
            node = TreeOption(branch.name,type,None)
            node = ctrl.AppendItem(root,str(node),-1,-1,
                                   wx.TreeItemData(node))
            self.convTree(ctrl,node,branch.value)

        elif branch.__class__ == CondNode:
            if len(branch.value) == 1:
                node = TreeCond(branch.field,branch.cond,branch.value[0])
                node = ctrl.AppendItem(root,str(node),-1,-1,
                                       wx.TreeItemData(node))
                self.convTree(ctrl,node,branch.block)
            else:
                # FIXME: Multiple values in cond is a HACK!!
                node = TreeCond(branch.field,branch.cond,None)
                node = ctrl.AppendItem(root,str(node),-1,-1,
                                       wx.TreeItemData(node))
                vals = ctrl.AppendItem(node,"Condition variables",-1,-1,
                                       wx.TreeItemData(TreeCondVars()))
                self.convTree(ctrl,vals,branch.value)
                self.convTree(ctrl,node,branch.block)
        else:
            raise Exception("UNKNOWN: " + str(branch) +":"+
                            str(branch.__class__))
        # }}}

    def notImpl(self,msg):
        help = wx.MessageDialog(self,msg, "Feature not implemented", wx.OK)
        help.ShowModal()
        help.Destroy()

    def SaveFile(self,event):
        flags = wx.FD_SAVE | wx.OVERWRITE_PROMPT | wx.FD_CHANGE_DIR 
        fileDialogue = wx.FileDialog(self,
                                     "Save config file..",
                                     "","config.cfg",
                                     "Lighttpd config file (*.cfg)|*.cfg"
                                     "|All files|*.*", flags)
        if fileDialogue.ShowModal() == wx.ID_OK:
            try:
                fileName = fileDialogue.GetPath()
                outFP = open(fileName,"w")
            except:
                self.errorDialog("Error","Unable to open file for writing")
            else:
                treeText = self.treeViewPanel.getTreeText()
                outFP.write(treeText)
                outFP.close()
        fileDialogue.Destroy()


    # Shows the 'About' dialog
    def ShowHelp(self,event):
        # {{{
        helpText = "LyteCFG v0.99a"
        help = wx.MessageDialog(self,helpText,"About LyteCFG",
                                wx.OK | wx.ICON_INFORMATION)
        help.ShowModal()
        help.Destroy()
        # }}}
        
    # Bail out!
    def Quit(self,event):
        self.Close(True)



